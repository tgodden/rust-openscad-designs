use openscad_builder::prelude::*;

pub struct RecordHook {
    sleeve_thickness: f32,
    wall_thickness: f32,
    size: f32,
    extruded_width: f32,
    slit_angel: [f32; 2],
    // tt = Thumb tack
    tt_diam: f32,
    tt_height: f32,
    tt_pin_diam: f32,
}

impl RecordHook {
    fn build(self) -> String {
        // # Don't change
        let tt_diam_eff = 1.1 * self.extruded_width + self.tt_diam;
        let tt_pin_diam_eff = 1.1 * self.extruded_width + self.tt_pin_diam;
        let back_size = f32::max(self.size, tt_diam_eff + self.wall_thickness);
        (0..4)
            .map(|m| {
                // Holder
                let base = Cuboid::new(
                    back_size,
                    back_size,
                    self.sleeve_thickness + 2.0 * self.wall_thickness,
                );
                let cutout = Cuboid::new(self.size, self.size, self.sleeve_thickness).translate((
                    self.wall_thickness,
                    self.wall_thickness,
                    self.wall_thickness,
                ));
                let front_diag =
                    Cuboid::new(self.size * 2.0, self.size * 2.0, self.wall_thickness + 0.2)
                        .rotate((0.0, 0.0, 45.0))
                        .translate((self.size + self.wall_thickness, self.wall_thickness, -0.1));
                let holder = (base - (cutout + front_diag)).translate((
                    -back_size / 2.0,
                    -back_size / 2.0,
                    0.0,
                ));

                // Thumb Tack
                let thumb_tack = (Cylinder::new(tt_diam_eff / 2.0, self.tt_height)
                    .set_resolution(32)
                    + Cylinder::new(tt_pin_diam_eff / 2.0, self.wall_thickness).set_resolution(32))
                .translate((
                    self.wall_thickness / 2.0,
                    self.wall_thickness / 2.0,
                    self.sleeve_thickness + self.wall_thickness,
                ));
                // Slit for inserting thumb tack
                let angel = self.slit_angel[m % 2];
                let slit =
                    Cuboid::new(self.tt_pin_diam, 2.0 * back_size, self.wall_thickness * 1.1)
                        .rotate((0.0, 0.0, angel))
                        .translate((
                            (self.tt_pin_diam - self.tt_pin_diam * angel.cos()) / 2.0,
                            (self.tt_pin_diam - self.tt_pin_diam * angel.sin()) / 2.0,
                            self.sleeve_thickness + self.wall_thickness * 0.95,
                        ));

                // Put everything together
                let hook = (holder - thumb_tack - slit)
                    .rotate((90.0, (270 + 90 * (m % 2)) as f32, 0.0))
                    .translate((
                        -back_size * 2.0 * ((m % 2) as f32),
                        -back_size * 2.0 * ((m / 2) as f32),
                        0.0,
                    ));
                hook.build()
            })
            .collect::<Vec<_>>()
            .join("\n")
    }
}

fn main() {
    let hook = RecordHook {
        // # Parameters (millimeters)
        sleeve_thickness: 4.0,
        wall_thickness: 1.0,
        size: 10.0,
        extruded_width: 0.15,
        slit_angel: [230.0, 0.0],
        // tt = Thumb tack
        tt_diam: 9.5,
        tt_height: 0.4,
        tt_pin_diam: 1.0,
    };
    println!("{}", hook.build())
}
